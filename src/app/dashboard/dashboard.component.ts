import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  data: any;
  options: any;
  basicData: any;
  basicOptions: any;

  



  constructor(private router: Router) {
  }


  Dashboard(dashboard: string) {

    this.router.navigate([`${dashboard}`])


  }

  

  ngOnInit(): void {
    this.basicData = {
      labels: ['12:00am', '', '', '', '', '', '11:59pm'],
      datasets: [
        {
          label: 'Second Dataset',
          data: [20, 15, 30, 50, 35, 15, 20],
          fill: false,
          borderColor: '#42A5F5',
          tension: .4
        }
      ]

    }

  }

 

 
}
