import { Component, Input, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
// import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  users = [];
  checked: string[] = [];
  items = ['Name', 'EmailAddress', 'Phone Number', 'DateCreated', 'Pin Address'];




  constructor(
    private appService: AppService,
    private router: Router,
  ) {

  }

  ngOnInit() {
    this.getUsersList();

    const TableData = [
      {
        "name": "Robert Helington",
        "email_address": "robert12@gmail.com",
        "phone_number": "123-456-7890",
        "date_created": "21-Dec-2020",
        "pincode_address": "0001893"
      },
      {
        "name": "Thomos H",
        "email_address": "thomas.h@gmail.com",
        "phone_number": "123-456-7890",
        "date_created": "21-Dec-2020",
        "pincode_address": "0001894"
      },
      {
        "name": "Robert C",
        "email_address": "crobert123@gmail.com",
        "phone_number": "123-456-7890",
        "date_created": "12-Feb-2020",
        "pincode_address": "0001895"
      },
      {
        "name": "James K",
        "email_address": "jamesk123@gmail.com",
        "phone_number": "123-456-7890",
        "date_created": "22-Nov-2020",
        "pincode_address": "0001896"
      },
      {
        "name": "peter Hathway",
        "email_address": "hathway@outlook.com",
        "phone_number": "123-456-7890",
        "date_created": "21-Dec-2020",
        "pincode_address": "0001896"
      },
      {
        "name": "Rose M",
        "email_address": "rose@gmail.com",
        "phone_number": "123-456-7890",
        "date_created": "21-Dec-2020",
        "pincode_address": "0001898"
      },
      {
        "name": "Thomos Peter",
        "email_address": "peter123@gmail.com",
        "phone_number": "123-456-7890",
        "date_created": "21-Dec-2020",
        "pincode_address": "0001899"
      },
      {
        "name": "Cristin jones",
        "email_address": "jones223@gmail.com",
        "phone_number": "123-456-7890",
        "date_created": "21-Dec-2020",
        "pincode_address": "0001900"
      },
      {
        "name": "John Marshal",
        "email_address": "jonemarshal@gmail.com",
        "phone_number": "123-456-7890",
        "date_created": "21-Dec-2020",
        "pincode_address": "0001901"
      },
      {
        "name": "Peter Hames",
        "email_address": "hames.p@gmail.com",
        "phone_number": "123-456-7890",
        "date_created": "21-Dec-2020",
        "pincode_address": "0001902"
      }
    ]
  }

  checktabledata(event: any) {
    console.log(event, this.users)
  }

  Userslist(userslist: string) {
    this.router.navigate([`${userslist}`])
  }

  getUsersList() {
    this.appService.getUserList().subscribe(
      response => this.users = response
    )
      ;
  }

  



}
