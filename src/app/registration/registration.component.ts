import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  get firstname() {
    return this.RegistrationForm.get('firstname');
  }

  get lastname() {
    return this.RegistrationForm.get('lastname');
  }
  get phonenumber() {
    return this.RegistrationForm.get('phonenumber');
  }
  get email() {
    return this.RegistrationForm.get('email');
  }
  get password() {
    return this.RegistrationForm.get('password');
  }
  get confirmpassword() {
    return this.RegistrationForm.get('confirmpassword');
  }

  Mustmatch(password: any, confirmpassword: any) {
    return (RegistrationForm: FormGroup) => {
      const passwordcontrol = RegistrationForm.controls[password];
      const confirmpasswordcontrol = RegistrationForm.controls[confirmpassword];
      if (confirmpasswordcontrol.errors && !confirmpasswordcontrol.errors['Mustmatch']) {
        return;
      }
      if (passwordcontrol.value !== confirmpasswordcontrol.value) {
        confirmpasswordcontrol.setErrors({ Mustmatch: true });
      } else {
        confirmpasswordcontrol.setErrors(null)
      }
    }

  }


  constructor(private fb: FormBuilder,
    private route: Router,
    private primengConfig: PrimeNGConfig,
    private messageService: MessageService,
  ) { }

  RegistrationForm: any = FormGroup;
  user: any

  ngOnInit() {
    this.primengConfig.ripple = true;
    this.RegistrationForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z]*')]],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z]*')]],
      phonenumber: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(10), Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.email]],
      myradio: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(15)]],
      confirmpassword: ['', [Validators.required]]
    }, {
      validators: this.Mustmatch('password', 'confirmpassword')
    })
  }


  HandleRegister() {
    console.log(this.RegistrationForm.value);
    this.RegistrationForm.markAllAsTouched()
    if (this.RegistrationForm.valid) {
      this.messageService.add({
        severity: 'success', summary: 'Success', detail: 'Submitted',
      });
      this.route.navigate(['/login'])

    } else {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Message Content' });
    }
    localStorage.setItem(this.RegistrationForm.value, JSON.stringify(this.RegistrationForm.value))
    console.log(localStorage.getItem(this.RegistrationForm.value))
  }

}
