import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService, PrimeNGConfig } from 'primeng/api';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  get firstname() {
    return this.ProfilePage.get('firstname');
  }
  get lastname() {
    return this.ProfilePage.get('lastname');
  }
  get phonenumber() {
    return this.ProfilePage.get('phonenumber');
  }
  get email() {
    return this.ProfilePage.get('email');
  }
  get pincode() {
    return this.ProfilePage.get('pincode');
  }


  constructor(private router: Router,
    private fb: FormBuilder,
    private primengConfig: PrimeNGConfig,
    private messageService: MessageService
  ) { }

  ProfilePage: any = FormGroup;


  Profile(profile: string) {

    this.router.navigate([`${profile}`])


  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    
    this.ProfilePage = this.fb.group({
      firstname:['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z]*')]],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z]*')]],
      phonenumber: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(10), Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.email]],
      pincode:['',[Validators.required]]
    });

   

  }

  ProfileLogin() {
    // console.log(this.ProfilePage.value);
    this.ProfilePage.markAllAsTouched()
    if (this.ProfilePage.valid) {
      this.messageService.add({
        severity: 'success', summary: 'Success', detail: 'Submitted',
      });
    } else {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Message Content' });
    }

  }

}
