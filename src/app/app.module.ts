import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { LoginComponent } from './login/login.component';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
// import { RegistrationComponent } from './registration/registration.component';
import { CardModule } from 'primeng/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { myRoutings } from './app-routing.module';
import { ButtonModule } from 'primeng/button';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { RippleModule } from 'primeng/ripple';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { ProfileComponent } from './profile/profile.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
// import { UserlistComponent } from './userlist/userlist.component';
import { CalendarModule } from 'primeng/calendar';
import { TableModule } from 'primeng/table';
import { CheckboxModule } from 'primeng/checkbox';
import { HttpClientModule } from '@angular/common/http';
import { DropdownModule } from 'primeng/dropdown';
import {ChartModule} from 'primeng/chart';
import { EmployeelistComponent } from './employeelist/employeelist.component';
import { ObservablesComponent } from './observables/observables.component';
import {DividerModule} from 'primeng/divider';













@NgModule({
  declarations: [
    AppComponent,
    myRoutings,
    EmployeelistComponent,
    ObservablesComponent,

    // ProfileComponent,
    // DashboardComponent,
    // UserlistComponent
    // LoginComponent,
    // RegistrationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    InputTextModule,
    PasswordModule,
    CardModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    RadioButtonModule,
    ToastModule,
    RippleModule,
    CalendarModule,
    TableModule,
    HttpClientModule,
    CheckboxModule,
    DropdownModule,
    ChartModule,
    DividerModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
