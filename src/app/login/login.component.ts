import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  get email() {

    return this.loginForm.get('email')
  }
  get password() {
    return this.loginForm.get('password')
  }

  constructor(private fb: FormBuilder,
    private primengConfig: PrimeNGConfig,
    private route: Router,
    private messageService: MessageService) { }

  loginForm: any = FormGroup;

  ngOnInit() {
    this.primengConfig.ripple = true;
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
      myradio: ['', Validators.required]
    });


  }



  onSubmit() {
    console.log(this.loginForm.value);
    this.loginForm.markAllAsTouched()
    if (this.loginForm.valid) {
      this.messageService.add({
        severity: 'success', summary: 'Success', detail: 'Submitted',
      });
      this.route.navigate(['/dashboard'])
    } else {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Message Content' });
    }
  }
}
