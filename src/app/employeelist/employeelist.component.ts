import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-employeelist',
  templateUrl: './employeelist.component.html',
  styleUrls: ['./employeelist.component.css']
})
export class EmployeelistComponent implements OnInit {

  myobservable = new Observable((test) => {
    test.next(1);
    test.next(2);
    test.next(4);
    test.error('test error');
    test.complete();
  })

  testObservable() {
    this.myobservable.subscribe(
      item => {
        console.log(item)
      },
      error => {
        console.log("error", error)
      },
      () => {
        console.log('completed')
      }
    )
  }


  employeelist: any;
  checked: string[] = [];
  items = ['Name', 'UserName', 'Email', 'PhoneNumber', 'Website', 'Action'];
  isCreateMode: boolean;
  employee: any;
  selectedgender: string = '';
  // gender: string[] = ['male', 'female'];

  constructor(private httpClient: HttpClient,

  ) {
    this.employeelist = [];
    this.isCreateMode = false
    this.employee = {
      name: '',
      username: '',
      email: '',
      phone: '',
      website: '',
      id: 1,
    }
  }

  ngOnInit(): void {
    this.getEmployeeList();
  }


  getEmployeeList() {
    this.httpClient.get('https://jsonplaceholder.typicode.com/users').subscribe((result: any) => {
      this.employeelist = result;
    })
  }
  edit(userId: any) {
    this.httpClient.get('https://jsonplaceholder.typicode.com/users/' + userId).subscribe((result: any) => {
      this.employee = result;
      this.AddUser();
    })
  }

  employeedata(event: any) {
    console.log(this.employeelist);
  }

  AddUser() {
    this.isCreateMode = true;
  }

  cancel() {
    this.isCreateMode = false
  }

  saveUser() {
    // debugger
    if (this.employee.id === 1) {
      this.httpClient.post('https://jsonplaceholder.typicode.com/users', this.employee).subscribe((result) => {
        alert("User Created Successfully")
      });
    } else {
      this.httpClient.put('https://jsonplaceholder.typicode.com/users/' + this.employee.id, this.employee).subscribe((result) => {
        alert("User Saved Successfully")


      });
    }

  }
  delete(userId: any) {
    this.httpClient.delete('https://jsonplaceholder.typicode.com/users/' + userId).subscribe((result: any) => {
      alert('User Deleted Successfully')

    });
  }
}
